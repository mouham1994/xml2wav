#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include "wave.h"

#define prl { printf("\n"); }

int main() {
  wave_t* w;

  printf("*** Generation of three seconds of silence (listen to it!) ***"); prl; prl;
  w = wave_create("silence.wav", 44100, 16, 1, 3 * 44100);
  wave_print_info(w);
  wave_close(w);
  prl;

  printf("*** Generation of a sine 440Hz (listen to it!) ***"); prl; prl;
  w = wave_create("sine440.wav", 44100, 16, 2, 3 * 44100);
  for(size_t k = 0; k < wave_get_B(w); k++) {
    double sig = sin(2.0 * M_PI * 440 * k / wave_get_f(w));
    double pan = 0.5 + 0.5 * cos(2.0 * M_PI * 0.5 * k / wave_get_f(w));
    wave_set(w, 0, k, pan * sig);
    wave_set(w, 1, k, (1.0 - pan) * sig);
  }
  wave_close(w);

  printf("*** Opening of WAVE file (should be equal to the one previously generated) ***"); prl; prl;
  w = wave_open("testwave.wav");
  printf("  - Information\n");
  printf("    - c: %lu\n", wave_get_c(w));
  printf("    - f: %lu\n", wave_get_f(w));
  printf("    - p: %lu\n", wave_get_p(w));
  printf("    - D: %lu\n", wave_get_D(w));
  printf("    - b: %lu\n", wave_get_b(w));
  printf("    - r: %lu\n", wave_get_r(w));
  printf("    - S: %lu\n", wave_get_S(w));
  printf("    - B: %lu\n", wave_get_B(w));
  prl;
  printf("  - Some samples\n");
  double s;
  s = 0.00; printf("    - %f: %e, %e\n", s, wave_get(w, 0, s), wave_get(w, 1, s));
  s = 0.12; printf("    - %f: %e, %e\n", s, wave_get(w, 0, s), wave_get(w, 1, s));
  s = 0.23; printf("    - %f: %e, %e\n", s, wave_get(w, 0, s), wave_get(w, 1, s));
  s = M_PI; printf("    - %f: %e, %e\n", s, wave_get(w, 0, s), wave_get(w, 1, s));
  wave_close(w);

  return EXIT_SUCCESS;
}
