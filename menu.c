#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "xml.h"
#include "wave.h"
#include "partition.h"
#include "menu.h"
#include "args.h"

//Variables globales
int count = 1;
score_t* part = NULL;
instrument_t** bank;
void f() { }

menu_t* createMenu(const char* text) {
	menu_t* menu = (menu_t*)malloc(sizeof(menu_t));
	menu->label = strdup(text);
	return menu;
}

void addMenuAction(menu_t* m, const char* text, void(*f)()) {
	item_t* item = (item_t*)malloc(sizeof(item_t));
	item->isAction = 1;
	
	action_t* action = (action_t*)malloc(sizeof(action_t));
	action->label = strdup(text);
	action->fun = f;
	
	item->action = action;
	item->next = m->content;
	m->content = item;
}

void addSubMenu(menu_t* m, menu_t* sm) {
	if (sm->father) return;
	sm->father = m;
	
	item_t* item = (item_t*)malloc(sizeof(item));
	item->isAction = 0;
	item->sub_menu = sm;
	
	item->next = m->content;
	m->content = item;
}

void deleteMenu(menu_t* m) {
	
	if (m) return;
	
	item_t* sons = m->content;
	
	while (sons) {
		
		if (!sons->isAction)
			deleteMenu(sons->sub_menu);
		
		item_t* copy_item = sons;
		sons = sons->next;
		free(copy_item);
	}
	
	free(m);
}

void launchMenu(menu_t* m) {
	if (m) {
		printf("%s\n", m->label);
		item_t* item = m->content;
		int i = 1;
		while (item) {
			if (item->isAction)
				printf("%d - %s\n", i, item->action->label);
			else
				printf("%d - %s\n", i, item->sub_menu->label);
			
			item = item->next;
			i++;
		}

		int choice;
		
		do {
			choice = getchar() - 48;
		} while (choice != 64 && choice != 32 && (choice < 1 || choice >= i));

		printf("\n");
		
		if (choice == 64 || choice == 32) {
			launchMenu(m->father);
		} else {
			int num = 1;
			item = m->content;
			while (item) {
				if (num == choice) {
					if (item->isAction) {
						item->action->fun();
					} else {
						launchMenu(item->sub_menu);
					}
					break;
				}
				num++;
				item = item->next;
			}
		}
	}
	
}


void newPart() {
	if (part) {
		// Proposer au user de sauvegarder l'ancienne
		printf("Une partition est deja ouverte !\n Souhaitez-vous la sauvegarder ?\n");
		printf("1 - Oui\n");
		printf("2 - Non\n");
		int choice;
		do {
			scanf("%d", &choice);
		} while (choice < 1 || choice > 2);
		if (choice == 1) {
			printf("Nom de la partition ? ");
			char name[100];
			scanf("%s", name);
			score_save_xml(part, name);
			printf("Sauvegarde reussie\n\n");
		}
		score_delete(part);
	}
	printf("\nCreation d'une nouvelle partition :\n");
	int tempo, duration, timeSignature, nbInstrument;
	printf("Tempo ? ");
	scanf("%d", &tempo);
	printf("Duration ? ");
	scanf("%d", &duration);
	printf("Time signature ? ");
	scanf("%d", &timeSignature);
	printf("Number of instruments ? ");
	scanf("%d", &nbInstrument);
	part = score((size_t)tempo, (size_t)duration, (size_t)timeSignature, (size_t)nbInstrument);
	printf("Creation reussie\n\n");
}

void save() {
	printf("Sauvegarde d'une partition ?\n");
	printf("Nom de la partition ? ");
	char name[100];
	scanf("%s", name);
	score_save_xml(part, name);
	printf("Sauvegarde reussie\n\n");
	score_delete(part);
}

void open() {
	if (part) {
		// Proposer au user de sauvegarder l'ancienne
		printf("Une partition est deja ouverte !\n Souhaitez-vous la sauvegarder ?\n");
		printf("1 - Oui\n");
		printf("2 - Non\n");
		int choice;
		do {
			scanf("%d", &choice);
		} while (choice < 1 || choice > 2);
		if (choice == 1) {
			printf("Nom de la partition ? ");
			char name[100];
			scanf("%s", name);
			score_save_xml(part, name);
			printf("Sauvegarde reussie");
		}
		score_delete(part);
	}
	printf("\nOuverture d'une partition :\n");
	printf("Nom du fichier xml ?");
	char name[100];
	scanf("%s", name);
	part = score_load_xml(name);
	printf("Ouverture reussie\n\n");
}

void information() {
	printf("Informations de la partition : ");
	score_print_info(part);
	printf("\n");
}

void add_note() {
	if (part) {
		printf("Ajout d'une note");
		int ins, bar, pitch, start, duration;
		
		printf("Instrument? ");
		scanf("%d", &ins);
		printf("Bar? ");
		scanf("%d", &bar);
		printf("Pitch? ");
		scanf("%d", &pitch);
		printf("Start? ");
		scanf("%d", &start);
		printf("Duration? ");
		scanf("%d", &duration);
		score_add_note(part, (size_t)ins, (size_t)bar, (size_t)pitch, (size_t)start, (size_t)duration);
		printf("Ajout de la note reussie");
	} else {
		printf("Creer ou charger une partition d'abord\n\n");
	}
}

void exit_prog() {
	if (part) {
		// Proposer au user de sauvegarder l'ancienne
		printf("Une partition est deja ouverte !\n Souhaitez-vous la sauvegarder ?\n");
		printf("1 - Oui\n");
		printf("2 - Non\n");
		int choice;
		do {
			scanf("%d", &choice);
		} while (choice < 1 || choice > 2);
		if (choice == 1) {
			printf("Nom de la partition ? ");
			char name[100];
			scanf("%s", name);
			score_save_xml(part, name);
			printf("Sauvegarde reussie");
		}
		score_delete(part);
	}
	count = 0;
}

void load() {
	printf("Chargement d'un instrument :\n");
	int num, pitch;
	char name[100];
	printf("Numero de l'instrument : ");
	scanf("%d", &num);
	printf("Pitch de ref : ");
	scanf("%d", &pitch);
	printf("Nom du fichier wave :");
	scanf("%s", name);
	bank[num] = instrument_sample(name, pitch);
	printf("Chargement reussi");
}

void unload() {
	printf("Dechargement d'un instrument :\n");
	int num;
	printf("Numero de l'instrument : ");
	scanf("%d", &num);
	bank[num] = NULL;
	printf("Dechargement reussi");
}

void exporter() {
	printf("Exporter au format wave ?\n");
	printf("Nom du fichier wave ? ");
	char name[100];
	scanf("%s", name);
	score_export_wave(part, bank, name);
	printf("Sauvegarde reussie\n\n");
	score_delete(part);
}

void menu() {
	bank = (instrument_t**) malloc(sizeof(instrument_t)*100);
	for (int i=0; i<10; i++) {
		bank[i] = NULL;
	}
	menu_t* mn;
	menu_t* file;
	menu_t* score;
	menu_t* instruments;
	mn = createMenu("Bienvenue sur xml2wav");
	score = createMenu("score");
	addSubMenu(mn, score);
	addMenuAction(score, "Export", exporter);
	instruments = createMenu("Instruments");
	addSubMenu(score, instruments);
	addMenuAction(instruments, "Unload", unload);
	addMenuAction(instruments, "Load", load);
	addMenuAction(score, "Add note", add_note);
	file = createMenu("File");
	addSubMenu(mn, file);
	addMenuAction(file, "Quitter", exit_prog);
	addMenuAction(file, "Information", information);
	addMenuAction(file, "Save", save);
	addMenuAction(file, "Open", open);
	addMenuAction(file, "New", newPart);
	while (count) launchMenu(mn);
	deleteMenu(mn);
}

