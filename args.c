#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "args.h"

option_t* opt_void(option_t* l, const char* kw, void (*f)()) {
	option_t* opt = (option_t*) malloc(sizeof(option_t));
	opt->keyword = strdup(kw);
	opt->spec = OptVoid;
	opt->fct.opt_void = f;
	opt->next = l;
	return opt;
}

option_t* opt_int(option_t* l, const char* kw, void (*f)(int)) {
	option_t* opt = (option_t*) malloc(sizeof(option_t));
	opt->keyword = strdup(kw);
	opt->spec = OptInt;
	opt->fct.opt_int = f;
	opt->next = l;
	return opt;
}

option_t* opt_float(option_t* l, const char* kw, void (*f)(float)) {
	option_t* opt = (option_t*) malloc(sizeof(option_t));
	opt->keyword = strdup(kw);
	opt->spec = OptFloat;
	opt->fct.opt_float = f;
	opt->next = l;
	return opt;
}

option_t* opt_string(option_t* l, const char* kw, void (*f)(const char*)) {
	option_t* opt = (option_t*) malloc(sizeof(option_t));
	opt->keyword = strdup(kw);
	opt->spec = OptString;
	opt->fct.opt_str = f;
	opt->next = l;
	return opt;
}

void opt_delete(option_t* l) {
	option_t* save = l;
	while (l) {
		save = l;
		l = l->next;
		free(save);
	}
}

void process_options(option_t* l, int argc, char* *argv) {

	int i=0;

	while (i<argc) {
		
		//C'est une option
		if (argv[i][0] == '-') {

			
			option_t* ptr = l;
			
			while (ptr) {
				
				if (strcmp(strdup(argv[i]), ptr->keyword) == 0) {
					switch (ptr->spec) {
						case OptVoid:
							ptr->fct.opt_void();
							break;
						case OptInt:
							ptr->fct.opt_int(atoi(argv[i+1]));
							break;
						case OptString:
							ptr->fct.opt_str(argv[i+1]);
							break;
						case OptFloat:
							ptr->fct.opt_float(strtof(argv[i+1] ,NULL));
							break;
						default:
							printf("NOTHING");
					}
				}
				
				ptr = ptr->next;
			}
			i+= 2;
		} else {
			i += 1;
		}
		
	}
	
}

void f1(const char* str) {
  printf("F1: %s\n", str);
}
 
void f2(int i) {
  printf("F2: %d\n", i);
}
 
void f3() {
  printf("F3: no param\n");
}
 
void f4(float f) {
  printf("F4: %f\n", f);
}

