#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "xml.h"

xelement_t* create_xelement(const char* name) {
	xelement_t* new_xelement = (xelement_t*)malloc(sizeof(xelement_t));
	if (!new_xelement) {
		perror("Error in create_xelement");
		exit(EXIT_FAILURE);
	}
	new_xelement->name = strdup(name);
	new_xelement->nbr_elems = 0;
    return new_xelement;
}

void add_xattribute(xelement_t* e, const char* name, const char* value) {
	// Tester si l'attribut appartient a l'element
	xattribute_t* pointer = e->attributes;
	while (pointer) {
		if (strcmp(pointer->name, name) == 0) {
			// Dans le cas ou l'attribut de nom "name" appartient deja a l'element
			free(pointer->value);
			pointer->value = strdup(value);
			return;
		}
		pointer = pointer->next_attribute;
	}
	// Dans le cas ou l'attribut de nom "name" n'appartient pas a l'element
	xattribute_t* new_xattribute = (xattribute_t*)malloc(sizeof(xattribute_t));
	if (!new_xattribute) {
		perror("Error in add_xattribute");
		exit(EXIT_FAILURE);
	}
	new_xattribute->name = strdup(name);
	new_xattribute->value = strdup(value);
	new_xattribute->next_attribute = e->attributes;
	e->attributes = new_xattribute;
}

void add_sub_xelement(xelement_t* e, xelement_t* s) {
	if (s->father || s->brother || e->nbr_elems == -1) {
		perror("Error in add_sub_xelement");
		exit(EXIT_FAILURE);
	}
	s->father = e;
	s->brother = e->sons;
	e->sons = s;
	e->nbr_elems++;
}

void add_raw(xelement_t* e, const char* r) {
	if (e->nbr_elems > 0) {
		perror("Error in add_raw");
		exit(EXIT_FAILURE);
	}
	if (e->nbr_elems == -1) free(e->raw);
	else  e->nbr_elems = -1;
	e->raw = strdup(r);
}

void delete_xelement(xelement_t* e) {
	if (e) {
		free(e->name);
		xattribute_t* attributes = e->attributes;
		while (attributes) {
			xattribute_t* save = attributes;
			attributes = attributes->next_attribute;
			free(save);
		}
		delete_xelement(e->brother);
		if (e->nbr_elems == -1) free(e->raw);
		else delete_xelement(e->sons);
		free(e);
	}
}

const char* get_xelement_name(xelement_t* e) {
	return e->name;
}

const char* get_xelement_attribute_value(xelement_t* e, const char* attribute_name) {
	xattribute_t* pointer = e->attributes;
	while (pointer) {
		if (strcmp(pointer->name, attribute_name) == 0) {
			return pointer->value;
		}
		pointer = pointer->next_attribute;
	}
	return NULL;
}

const char* get_xelement_rawdata(xelement_t* e) {
	if (e->nbr_elems == -1) return e->raw;
	return NULL;
}

xelement_t* get_xelement_children(xelement_t* e) {
	if (e->nbr_elems <= 0) return NULL;
	return e->sons;
}

xelement_t* next_xelement_child(xelement_t* e) {
	return e->brother;
}

void save_xelement(FILE* fd, xelement_t* e) {
	if (e) {
		fprintf(fd, "<%s", e->name);
		xattribute_t* attribute = e->attributes;
		while (attribute) {
			fprintf(fd, " %s=\"%s\"", attribute->name, attribute->value);
			attribute = attribute->next_attribute;
		}
		if (e->nbr_elems == 0) {
			fprintf(fd, " />\n");
		} else {
			if (e->nbr_elems == -1) {
				fprintf(fd, ">");
				fprintf(fd, "%s", e->raw);
			} else {
				fprintf(fd, ">\n");
				save_xelement(fd, e->sons);
			}
			fprintf(fd, "</%s>\n", e->name);
		}
		save_xelement(fd, e->brother);
	}
}

void save_xml(const char* fname, xelement_t* e) {
	FILE* fd = fopen(fname, "w");
	if (!fd) {
		perror("Error in save_xml");
		exit(EXIT_FAILURE);
	}
	save_xelement(fd, e);
	fclose(fd);
}

void print_xelement(xelement_t* e) {
	save_xelement(stdout, e);
}

int is_space_char(char c) {
	return (c == ' ' || c == '\n' || c == '\r' || c == '\t');
}

int is_special_char_xml(char c) {
	return (c == '<' || c == '>' || c == '/' || c == '=');
}

char next_char(FILE* fd) {
	char c;
	do {
		c = fgetc(fd);
	} while (is_space_char(c));
	if(c == EOF) {
		perror("Error in next_char");
		exit(EXIT_FAILURE);
	}
	return c;
}

void check_next_char(FILE* fd, char c) {
	char next = next_char(fd);
	if (next != c) {
		perror("Error in check_next_char");
		exit(EXIT_FAILURE);
	}
}

int is_next_char(FILE* fd, char c, int cons) {
	char next = next_char(fd);
	char tmp[1];
	tmp[0] = next;
	
	if (next == c) {
		if (!cons) ungetc(next, fd);
		return 1;
	}
	
	ungetc(next, fd);
	return 0;
}

char* next_word(FILE* fd) {
	int size = 10;
	char* word = (char*)malloc(sizeof(char)*size);
	int i = 0;
	char c = next_char(fd);
	while (!is_space_char(c) && !is_special_char_xml(c) && c != EOF) {
		if (i == size) {
			size *= 2;
			word = (char*)realloc(word, sizeof(char)*size);
		}
		word[i] = c;
		i++;
		c = fgetc(fd);
	}
	if (c == EOF) {
		perror("Error in next_word");
		exit(EXIT_FAILURE);
	}
	ungetc(c, fd);
	word[i] = '\0';
	return word;
}

void check_next_word(FILE* fd, const char* w) {
	char* next = next_word(fd);
	if (strcmp(w, next) != 0) {
		perror("Error in check_next_word");
		exit(EXIT_FAILURE);
	}
}

char* next_string(FILE* fd) {
	
	char c = next_char(fd);
	
	if (c != '"') {
		perror("");
		exit(EXIT_FAILURE);
	}
	
	int size = 10;
	char* word = (char*)malloc(sizeof(char)*size);
	int i = 0;
	
	c = fgetc(fd);
	
	while (c != '"' && c != EOF) {
		if (i == size) {
			size *= 2;
			word = (char*)realloc(word, sizeof(char)*size);
		}
		word[i] = c;
		i++;
		c = fgetc(fd);
	}
	
	if (c == EOF) {
		perror("Error in next_word");
		exit(EXIT_FAILURE);
	}
	
	word[i] = '\0';
	return word;
}

char* next_raw(FILE* fd) {

	int size = 10;
	char* word = (char*)malloc(sizeof(char)*size);
	int i = 0;
	
	char c = next_char(fd);
	
	while (c != '<' && c != EOF) {
		if (i == size) {
			size *= 2;
			word = (char*)realloc(word, sizeof(char)*size);
		}
		word[i] = c;
		i++;
		c = fgetc(fd);
	}
	if (c == EOF) {
		perror("Error in next_word");
		exit(EXIT_FAILURE);
	}
	ungetc(c, fd);
	word[i] = '\0';
	return word;
}

void load_xelement_raw(FILE* fd, xelement_t* e) {
  char* w = next_raw(fd);
  check_next_char(fd, '<');
  check_next_char(fd, '/');
  check_next_word(fd, e->name);
  check_next_char(fd, '>');
  add_raw(e,w);
  free(w);
}

void load_xelement_sub(FILE* fd, xelement_t* e) {
  xelement_t* f = load_xelement(fd, e->name);
  if (f != NULL) {
    add_sub_xelement(e,f);
    load_xelement_sub(fd, e);
  }
}

void load_xelement_content(FILE* fd, xelement_t* e) {
	if (is_next_char(fd,'<',0)) {
	  load_xelement_sub(fd, e);
	 }
	else
	  load_xelement_raw(fd, e);
}

xelement_t* load_xelement(FILE* fd, const char* end_tag) {
	xelement_t* e = NULL;
	char c;

	check_next_char(fd,'<');

	if ((end_tag) && (is_next_char(fd,'/',1))) {
		check_next_word(fd,end_tag);
		check_next_char(fd,'>');
		return NULL;
	}

	char* name = next_word(fd);
	if (name == NULL) {
		fprintf(stderr, "load_xelement: tag name expected\n");
		exit(EXIT_FAILURE);
	}
	e = create_xelement(name);
	free(name);
	
		
	name = next_word(fd);
	
	while (name != NULL && name[0] != '\0') {
		check_next_char(fd,'=');
		char* value = next_string(fd);
		add_xattribute(e,name,value);
		free(name);
		free(value);
		// Erreur dans les fichiers fournis par le prof, l'instruction ci-dessus est obligatoire
		name = next_word(fd);
	}

	c = next_char(fd);
	char tmp[1];
	tmp[0] = c;
	

	if (c == '/') {
		check_next_char(fd,'>');
		return e;
	}

	if (c == '>') {
		load_xelement_content(fd, e);
		return e;
	}

	fprintf(stderr, "load_xelement: end of markup expected ('>' or '/>'), but got %c\n", c);
	exit(EXIT_FAILURE);
}

xelement_t* load_xml(const char* fname) {
  FILE* fd = fopen(fname, "r");
  xelement_t* e = load_xelement(fd,NULL);
  fclose(fd);
  return e;
}
