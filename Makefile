all: xml.o wave.o partition.o menu.o args.o main.o
	gcc -o xml2wave xml.o wave.o partition.o menu.o args.o main.o -lm
	

testxml: testxml.o xml.o
	gcc -o testxml testxml.o xml.o

testwave: testwave.o wave.o
	gcc -o testwave testwave.o wave.o -lm

testpartition: testpartition.o partition.o xml.o wave.o
	gcc -o testpartition testpartition.o partition.o xml.o wave.o -lm

testargs: testargs.o args.o
	gcc -o testargs testargs.o args.o


main.o: xml.c wave.c partition.c menu.c args.c main.c
	gcc -o main.o -c main.c -lm
	
menu.o: xml.c wave.c partition.c menu.c args.c main.c
	gcc -o menu.o -c menu.c -lm

testxml.o: testxml.c
	gcc -o testxml.o -c testxml.c

xml.o: xml.c
	gcc -o xml.o -c xml.c

testwave.o: testwave.c
	gcc -o testwave.o -c testwave.c -lm

wave.o: wave.c
	gcc -o wave.o -c wave.c -lm


testpartition.o: testpartition.c
	gcc -o testpartition.o -c testpartition.c -lm
	
partition.o: partition.c
	gcc -o partition.o -c partition.c -lm

testargs.o: testargs.c
	gcc -o testargs.o -c testargs.c
	
args.o: args.c
	gcc -o args.o -c args.c

clean:
	rm -rf *.o
