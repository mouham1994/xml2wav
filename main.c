#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "xml.h"
#include "wave.h"
#include "partition.h"
#include "menu.h"
#include "args.h"

score_t* PART = NULL;
char *wave_name;
instrument_t** BANK;

// L'option -i
void load1(const char* fname) {
	PART = score_load_xml(fname);
}

// L'option -o
void renam(const char* fname) {
	free(wave_name);
	wave_name = strdup(fname);
}

// L'option -ins
void load2() {
	printf("hello");
}


int main(int argc, char** argv) {
	
	if (argc <= 1) {
		menu();
		
	} else {
		
		option_t* opt = NULL;
		wave_name = strdup("out.wave"); // INIT Name of the wave
		BANK = (instrument_t**) malloc(sizeof(instrument_t)*100); // Init BANK
		for (int i=0; i<100; i++) {
			BANK[i] = NULL;
		}
  
		opt = opt_string(opt, "-i", load1);
		opt = opt_string(opt, "-o", renam);
		//opt = opt_void(opt, "-ins", load2);

		process_options(opt, argc, argv);

		opt_delete(opt);
		
		// Conversion from xml to wave
		score_export_wave(PART, BANK, wave_name);
		
	}
	
	
	
	
	return EXIT_SUCCESS;
}
