#ifndef __MIDI2WAV_WAVE__
#define __MIDI2WAV_WAVE__
 
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
 
// Types

typedef struct wave_s {
	uint16_t c;
	uint32_t f;
	uint16_t p;
	uint32_t B;
	FILE* wave_f;
} wave_t;
 
// Constructors
 
wave_t* wave_open(const char* fname);
 
wave_t* wave_create(const char* fname, size_t f, size_t p, size_t c, size_t B);
 
// Destructors
 
void wave_close(wave_t* wave);
 
// Getters / Setters
 
void wave_print_info(wave_t* wave);
 
size_t wave_get_c(wave_t* wave);
 
size_t wave_get_f(wave_t* wave);
 
size_t wave_get_p(wave_t* wave);
 
size_t wave_get_D(wave_t* wave);
 
size_t wave_get_b(wave_t* wave);
 
size_t wave_get_r(wave_t* wave);
 
size_t wave_get_S(wave_t* wave);
 
size_t wave_get_B(wave_t* wave);
 
void wave_set(wave_t* w, size_t ch, size_t k, double sig);
 
double wave_get(wave_t* w, size_t ch, double s);
 
#endif
