#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "partition.h"
#include "xml.h"
#include "wave.h"

pitch_t pitch(pitchclass_t c, char o) {
	return 12 * (o + 1) + c;
}

char pitch_octave(pitch_t n) {
	return n/12 - 1;
}

pitchclass_t pitch_class(pitch_t h) {
	return h % 12;	
}

void pitch_print(pitch_t h) {
	pitchclass_t pc = pitch_class(h);
	char po = pitch_octave(h);

	switch (pc) {
		case 0: printf("C"); break;
		case 1: printf("C#"); break;
		case 2: printf("D"); break;
		case 3: printf("D#"); break;
		case 4: printf("E"); break;
		case 5: printf("F"); break;
		case 6: printf("F#"); break;
		case 7: printf("G"); break;
		case 8: printf("G#"); break;
		case 9: printf("A"); break;
		case 10: printf("A#"); break;
		default: printf("Unkown");
	}
	
	printf("%d", po);
	
}

double pitch_frequency(pitch_t h) {
	return 440 * pow(R, h-69);
}

score_t* score(size_t tempo, size_t duration, size_t sig, size_t nbInstr) {
	score_t* sc = (score_t*)malloc(sizeof(score_t));
	sc->tempo = tempo * 60;
	sc->duration = duration;
	sc->timeSignature = sig;
	sc->nbInstrument = nbInstr;
	sc->instruments = (staff_t*)malloc(sizeof(staff_t) * nbInstr);
	for (int i=0; i<nbInstr; i++) {
		sc->instruments[i].size = 1;
		sc->instruments[i].bars = (bar_t*) malloc(sizeof(bar_t));
	}
	return sc;
}

void score_add_note(score_t* sc, size_t ins, size_t bar, pitch_t p, size_t start, size_t duration) {

	note_t* nt = (note_t*)malloc(sizeof(note_t));
	nt->start = start;
	nt->duration = duration;
	nt->pitch = p;

	bar_t* b = sc->instruments[ins].bars;
	if (sc->instruments[ins].size < bar) {
		sc->instruments[ins].bars = realloc(sc->instruments[ins].bars, bar);
	}
	nt->next = b[bar].notes;
	b[bar].notes = nt;
}

void score_delete(score_t* sc) {
	for (int i=0; i<sc->nbInstrument; i++) {
		free(sc->instruments[i].bars);
	}
	free(sc->instruments);
}

void score_print_info(score_t* sc) {
	printf("Tempo: %d beat(s) per minute\n", (int)sc->tempo/60);
	printf("Duration: %f sec\n", (double)sc->duration);
	printf("Instruments(s): %d\n", (int)sc->nbInstrument);
	for (int i=0; i<sc->nbInstrument; i++) {
		printf("\t* Instrument %d\n", i);
		for (int j=0; j<sc->instruments[i].size; j++) {
			printf("\t\t- Bar %d: ", j);
			note_t* pointer = sc->instruments[i].bars[j].notes;
			while (pointer) {
				pitch_print(pointer->pitch);
				printf(" ");
				pointer = pointer->next;
			}
			printf("\n");
		}
		printf("\n");
	}
	
}

void score_save_xml(score_t* sc, const char* fname) {
	FILE* f = fopen(fname, "w");
	fprintf(f, "<score tempo=\"%d\" duration=\"%d\" timeSignature=\"%d\" nbInstrument=\"%d\">\n", (int)sc->tempo, (int)sc->duration, (int)sc->timeSignature, (int)sc->nbInstrument);
	for (int i=0; i<sc->nbInstrument; i++) {
		fprintf(f, "<instrument id= \"%d\">\n", i);
		for (int j=0; j<sc->instruments[i].size; j++) {
			fprintf(f, "<bar id= \"%d\">\n", j);
			note_t* notes = sc->instruments[i].bars[j].notes;
			while (notes != NULL) {
				fprintf(f, "<note pitch=\"%d\" start=\"%d\" duration=\"%d\" />\n", (int)notes->pitch, (int)notes->start, (int)notes->duration);
				notes = notes->next;
			}
			fprintf(f, "</bar>\n");
		}
		fprintf(f, "</instrument>\n");
	}
	fprintf(f, "</score>\n");
	
	fclose(f);
}

score_t* score_load_xml(const char* fname) {
	xelement_t* xml = load_xml(fname);
	score_t* sc = (score_t*)malloc(sizeof(score_t));
	
	xattribute_t* attributes = xml->attributes;
	while (attributes) {
		
		if (strcmp(attributes->name, "tempo") == 0) {
			sc->tempo = (size_t) atoi(attributes->value);
		} else if (strcmp(attributes->name, "duration") == 0) {
			sc->duration = (size_t) atoi(attributes->value);
		} else if (strcmp(attributes->name, "timeSignature") == 0) {
			sc->timeSignature = (size_t) atoi(attributes->value);
		} else if (strcmp(attributes->name, "nbInstrument") == 0) {
			sc->nbInstrument = (size_t) atoi(attributes->value);
		}
		
		attributes = attributes->next_attribute;
	}
	
	sc->instruments = (staff_t*)malloc(sizeof(staff_t) * sc->nbInstrument);
	
	xelement_t* instrument = xml->sons;
	int i=0;
	
	while (instrument) {
		
		//compter le nombre de bars
		xelement_t* bar = instrument->sons;
		xelement_t* copy_bar = bar;
		int j=0;
		while (copy_bar) { copy_bar = copy_bar->brother; j++; }
		
		sc->instruments[i].size = j;
		sc->instruments[i].bars = (bar_t*) malloc(sizeof(bar_t) * j);
		
		
		// Remplir les bars
		j = 0;
		while (bar) {
			sc->instruments[i].bars[j].notes = NULL;
			xelement_t* note = bar->sons;
			
			while (note) {
				
				xattribute_t* att = note->attributes;
				size_t pitch, start, duration;
				while (att) {
					if (strcmp(att->name, "pitch") == 0) {
						pitch = (size_t) atoi(att->value);
					} else if (strcmp(att->name, "start") == 0) {
						start = (size_t) atoi(att->value);
					} else if (strcmp(att->name, "duration") == 0) {
						duration = (size_t) atoi(att->value);
					}
					att = att->next_attribute;
				}
				
				score_add_note(sc, i, j, pitch, start, duration);
				
				note = note->brother;
			}
			
			j++;
			bar = bar->brother;
		}
		
		i++;
		instrument = instrument->brother;
	}	
	
	return sc;
}

instrument_t* instrument_signal(double (*sig)(double freq, double sec)) {
	instrument_t* ins = (instrument_t*)malloc(sizeof(instrument_t));
	ins->isWave = 0;
	ins->sig = sig;
	ins->vol = 1.0;
	ins->attaque = 0.0;
	ins->duration = INFINITY;
	return ins;
}

instrument_t* instrument_sample(const char* fname, pitch_t pitch_ref) {
	instrument_t* ins = (instrument_t*)malloc(sizeof(instrument_t));
	ins->isWave = 1;
	ins->vol = 1.0;
	ins->attaque = 0.0;
	ins->duration = INFINITY;
	ins->pitch = pitch_ref;
	ins->w = wave_open(fname);
	return ins;
}

void instrument_delete(instrument_t* ins) {
	if (ins->isWave) {
		wave_close(ins->w);
	}
	free(ins);
}

void instrument_set_volume(instrument_t* ins, double volume) {
	ins->vol = volume;
}

void instrument_set_attack(instrument_t* ins, double attack) {
	ins->attaque = attack;
}

void instrument_set_duration(instrument_t* ins, double duration) {
	ins->duration = duration;
}

double instrument_play(instrument_t* ins, pitch_t n, double s) {
	if (ins->isWave) {
		return 0.0; //
	} else {
		return ins->sig(pitch_frequency(n), s);
	}
}

void score_export_wave(score_t* sc, instrument_t** sounds_bank, const char* fname) {
  size_t tempo    = sc->tempo ;   // Tempo of score sc
  size_t duration = sc->duration ;   // Duration (= number of bars) of score sc
  size_t timeSig  = sc->timeSignature ;   // Time signature of score sc
  size_t nbInstr  = sc->nbInstrument ;   // Number of instruments of score sc
 
 
  size_t freq = 44100;                                   // Desired sampling frequency
  size_t splPerTick = freq / tempo;                      // Number of samples per tick
  freq = splPerTick * tempo;                             // Real sampling frequency
  size_t nbSample = splPerTick * duration * timeSig;     // Total number of samples
 
  double period = 1.0 / freq;
 
  wave_t* w = wave_create(fname, freq, 16, 1, nbSample);
 
  size_t ss = 0;
  for(size_t b = 0; b < duration; b++) {
    for(size_t t = 0; t < timeSig; t++) {
      for(size_t s = 0; s < splPerTick; s++) {
        double x = 0;
        for(size_t i = 0; i < nbInstr; i++) {
          if (sounds_bank[i] == NULL) continue;
          
		note_t* pointer = sc->instruments[i].bars[b].notes;
		
          while (pointer) { // For each note n of bar b of instrument i (can be a while loop)
            size_t  n_start    = pointer->start ;  // Start tick of note n
            size_t  n_duration = pointer->duration ;  // Duration of note n in ticks
            pitch_t n_pitch    = pointer->pitch ;  // Pitch of note n
            if (n_start <= t && t < n_start + n_duration) {
              x += instrument_play(sounds_bank[i], n_pitch, period * (s + splPerTick * (t - n_start)));
            }
            pointer = pointer->next;
          }
        }
        wave_set(w, 0, ss, x);
        ss++;
      }
    }
  }
 
  wave_close(w);
}
