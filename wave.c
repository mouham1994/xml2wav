#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include "wave.h"

typedef unsigned char uint4_t;

void exit_failure(const char* msg) {
	perror(msg);
	exit(EXIT_FAILURE);
}

wave_t* wave_open(const char* fname) {
	
	FILE* f = fopen(fname, "rb+");
	int nbre;
	
	//"RIFF"
	char riff[4];
	nbre = fread(riff, sizeof(char), 4, f);
	riff[4] = '\0';
	if (nbre != 4 || strcmp(riff, "RIFF") != 0) exit_failure("RIFF not found");
	//printf("RIFF = %s\n", riff);
	
	//S
	uint32_t S[1];
	nbre = fread(S, sizeof(uint32_t), 1, f);
	if (nbre != 1) exit_failure("S not found");
	//printf("S = %ld\n", (long)S[0]);
	
	//"WAVE"
	char wave[4];
	nbre = fread(wave, sizeof(char), 4, f);
	wave[4] = '\0';
	if (nbre != 4 || strcmp(wave, "WAVE") != 0) exit_failure("WAVE not found");
	//printf("WAVE = %s\n", wave);
	
	//"fmt "
	char fmt[4];
	nbre = fread(fmt, sizeof(char), 4, f);
	fmt[4] = '\0';
	if (nbre != 4 || strcmp(fmt, "fmt ") != 0) exit_failure("fmt  not found");
	//printf("fmt = %s\n", fmt);
	
	//Nombre
	uint32_t Nombre[1];
	nbre = fread(Nombre, sizeof(uint32_t), 1, f);
	if (nbre != 1 || Nombre[0] != 16) exit_failure("Nombre not found");
	//printf("Nombre = %ld\n", (long)Nombre[0]);
	
	//PCM
	uint16_t PCM[1];
	nbre = fread(PCM, sizeof(uint16_t), 1, f);
	if (nbre != 1 || PCM[0] != 1) exit_failure("PCM not found");
	//printf("PCM = %ld\n", (long)PCM[0]);
	
	//c
	uint16_t c[1];
	nbre = fread(c, sizeof(uint16_t), 1, f);
	if (nbre != 1 || c[0] < 1 || c[0] > 6) exit_failure("c not found");
	//printf("c = %ld\n", (long)c[0]);
	
	//f
	uint32_t fq[1];
	nbre = fread(fq, sizeof(uint32_t), 1, f);
	if (nbre != 1) exit_failure("f not found");
	//printf("fq = %ld\n", (long)fq[0]);
	
	//r
	uint32_t r[1];
	nbre = fread(r, sizeof(uint32_t), 1, f);
	if (nbre != 1) exit_failure("r not found");
	//printf("r = %ld\n", (long)r[0]);
	
	//b
	uint16_t b[1];
	nbre = fread(b, sizeof(uint16_t), 1, f);
	if (nbre != 1) exit_failure("b not found");
	//printf("b = %ld\n", (long)b[0]);
	
	//p
	uint16_t p[1];
	nbre = fread(p, sizeof(uint16_t), 1, f);
	if (nbre != 1 || (p[0] != 8 && p[0] != 16 && p[0] != 24)) exit_failure("f not found");
	//printf("p = %ld\n", (long)p[0]);
	
	//"data"
	char data[4];
	nbre = fread(data, sizeof(char), 4, f);
	data[4] = '\0';
	if (nbre != 4 || strcmp(data, "data") != 0) exit_failure("data not found");
	//printf("data = %s\n", data);
	
	//D
	uint32_t D[1];
	nbre = fread(D, sizeof(uint32_t), 1, f);
	if (nbre != 1) exit_failure("D not found");
	//printf("D = %ld\n", (long)D[0]);
	
	/*
		uint16_t c;
		uint32_t f;
		uint16_t p;
		uint32_t B;
		FILE* wave_f;
	*/
	
	// Verifier le nombre d'octets de donnees dans le fichier wave
	uint4_t BUF[1];
	int count = 0;
	while (fread(BUF, sizeof(uint4_t), 1, f)) count++;
	if (count != D[0]) exit_failure("D != Number of DATA in file");
	
	//printf("count = %d\n", count);
	
	rewind(f);
	
	wave_t* w = (wave_t*)malloc(sizeof(wave_t));
	
	w->c = c[0];
	w->f = fq[0];
	w->p = p[0];
	w->B = D[0]/b[0];
	w->wave_f = f;
	
	return w;
}

wave_t* wave_create(const char* fname, size_t f, size_t p, size_t c, size_t B) {
	
	FILE* file = fopen(fname, "wb+");
	uint16_t b = ((uint16_t) c) * ((uint16_t) p) / 8;
	uint32_t r = ((uint32_t) f) * b;
	uint32_t D = b * ((uint32_t) B);
	uint32_t S = D + 44 - 8;
	uint32_t i32[1];
	uint16_t i16[1];
	
	//RIFF
	char* buf = strdup("RIFF");
	fwrite(buf, sizeof(char), 4, file);
	free(buf);
	
	//S
	i32[0] = S;
	fwrite(i32, sizeof(uint32_t), 1, file);
	
	//WAVE
	buf = strdup("WAVE");
	fwrite(buf, sizeof(char), 4, file);
	free(buf);
	
	//fmt
	buf = strdup("fmt ");
	fwrite(buf, sizeof(char), 4, file);
	free(buf);
	
	//Nombre 16
	i32[0] = (uint32_t) 16;
	fwrite(i32, sizeof(uint32_t), 1, file);
	
	//PCM
	i16[0] = (uint16_t) 1;
	fwrite(i16, sizeof(uint16_t), 1, file);
	
	//c
	i16[0] = (uint16_t) c;
	fwrite(i16, sizeof(uint16_t), 1, file);
	
	//f
	i32[0] = (uint32_t) f;
	fwrite(i32, sizeof(uint32_t), 1, file);
	
	//r
	i32[0] = r;
	fwrite(i32, sizeof(uint32_t), 1, file);
	
	//b
	i16[0] = (uint16_t) b;
	fwrite(i16, sizeof(uint16_t), 1, file);
	
	//p
	i16[0] = p;
	fwrite(i16, sizeof(uint16_t), 1, file);
	
	//data
	buf = strdup("data");
	fwrite(buf, sizeof(char), 4, file);
	//free(buf);
	
	//D
	i32[0] = D;
	fwrite(i32, sizeof(uint32_t), 1, file);
	
	uint4_t BUF[1];
	BUF[0] = 0;
	uint32_t count = 0;
	while (count < D) {
		fwrite(BUF, sizeof(uint4_t), 1, file);
		count++;
	}
	
	rewind(file);
	
	wave_t* w = (wave_t*)malloc(sizeof(wave_t));
	
	w->c = (uint16_t) c;
	w->f = (uint32_t) f;
	w->p = (uint16_t) p;
	w->B = (uint32_t) B;
	w->wave_f = file;
	
	return w;
}

void wave_close(wave_t* wave) {
	fclose(wave->wave_f);
	free(wave);
}

void wave_print_info(wave_t* w) {
	
	uint16_t b = w->c * w->p / 8;
	uint32_t r = w->f * b;
	uint32_t D = b * w->B;
	uint32_t S = D + 44 - 8;
	
	printf("Number of channels: %ld\n", (long) w->c);
	printf("Sample rate: %ld\n", (long) r);
	printf("Precision: %ld\n", (long) w->p);
	printf("Number of samples: %ld\n", (long) S);

}

size_t wave_get_c(wave_t* wave) {
	return wave->c;
}
 
size_t wave_get_f(wave_t* wave) {
	return wave->f;
}
 
size_t wave_get_p(wave_t* wave) {
	return wave->p;
}
 
size_t wave_get_D(wave_t* wave) {
	uint16_t b = wave->c * wave->p / 8;
	uint32_t D = b * wave->B;
	return D;
}
 
size_t wave_get_b(wave_t* wave) {
	uint16_t b = wave->c * wave->p / 8;
	return b;
}

size_t wave_get_r(wave_t* wave) {
	uint16_t b = wave->c * wave->p / 8;
	uint32_t r = wave->f * b;
	return r;
}
 
size_t wave_get_S(wave_t* wave) {
	uint16_t b = wave->c * wave->p / 8;
	uint32_t D = b * wave->B;
	uint32_t S = D + 44 - 8;
	return S;
}
 
size_t wave_get_B(wave_t* wave) {
	return wave->B;
}

void wave_set(wave_t* w, size_t ch, size_t k, double sig) {
	uint32_t b_pos = 44 + k * wave_get_b(w);
	fseek(w->wave_f, b_pos, SEEK_SET);
	sig = sig < -1 ? -1:sig;
	sig = sig > 1 ? 1:sig;
	if (w->p == 8) {
		uint8_t buf[wave_get_b(w)];
		fread(buf, sizeof(uint8_t), wave_get_b(w), w->wave_f);
		buf[ch] = (int8_t)(sig * INT8_MAX);
		rewind(w->wave_f);
		fseek(w->wave_f, b_pos, SEEK_SET);
		fwrite(buf, sizeof(uint8_t), wave_get_b(w), w->wave_f); 
	} else {
		uint16_t buf[wave_get_b(w)];
		fread(buf, sizeof(uint16_t), wave_get_b(w), w->wave_f);
		buf[ch] = (int16_t)(sig * INT16_MAX);
		rewind(w->wave_f);
		fseek(w->wave_f, b_pos, SEEK_SET);
		fwrite(buf, sizeof(int16_t), wave_get_b(w), w->wave_f); 
	}
	
}

double wave_get(wave_t* w, size_t ch, double s) {
	
	uint32_t p1 = ((int)floor(s * w->f)) % w->B;
	uint32_t p2 = ((int)ceil(s * w->f)) % w->B;
	
	uint32_t b_p1 = 44 + p1 * wave_get_b(w);
	uint32_t b_p2 = 44 + p2 * wave_get_b(w);
	
	double d1, d2;
	
	if (w->p == 8) {
		
		rewind(w->wave_f);
		fseek(w->wave_f, b_p1, SEEK_SET);
		uint8_t buf1[wave_get_b(w)];
		fread(buf1, sizeof(uint8_t), wave_get_b(w), w->wave_f);
		
		rewind(w->wave_f);
		fseek(w->wave_f, b_p2, SEEK_SET);
		uint8_t buf2[wave_get_b(w)];
		fread(buf2, sizeof(uint8_t), wave_get_b(w), w->wave_f);
		
		uint8_t i1 = buf1[ch];
		uint8_t i2 = buf2[ch];
		
		d1 = ((double) i1) / INT8_MAX;
		d2 = ((double) i2) / INT8_MAX;
		
	} else {
		
		rewind(w->wave_f);
		fseek(w->wave_f, b_p1, SEEK_SET);
		uint16_t buf1[wave_get_b(w)];
		fread(buf1, sizeof(uint16_t), wave_get_b(w), w->wave_f);
		
		rewind(w->wave_f);
		fseek(w->wave_f, b_p2, SEEK_SET);
		uint16_t buf2[wave_get_b(w)];
		fread(buf2, sizeof(uint16_t), wave_get_b(w), w->wave_f);
		
		uint16_t i1 = buf1[ch];
		uint16_t i2 = buf2[ch];
		
		d1 = ((double) i1) / INT16_MAX;
		d2 = ((double) i2) / INT16_MAX;
		 
	}
	
	rewind(w->wave_f);
	
	return (ceil(s*w->f)-s*w->f) * d1 + (s*w->f - floor(s*w->f)) * d2;
}
