#ifndef __MIDI2WAV_MENU__
#define __MIDI2WAV_MENU__

#include <stdio.h>

typedef struct action_s {
	char* label;
	void (*fun)();
} action_t;

typedef struct item_s item_t;

typedef struct menu_s {
	struct menu_s* father;
	char* label;
	item_t* content;
} menu_t;

struct item_s {
	int isAction;
	union {
		menu_t* sub_menu;
		action_t* action;
	};
	item_t* next;
};

menu_t* createMenu(const char* text);
void addMenuAction(menu_t* m, const char* text, void(*f)());
void addSubMenu(menu_t* m, menu_t* sm);
void deleteMenu(menu_t* m);
void launchMenu(menu_t* m);

void newPart();
void save();
void open();
void information();
void add_note();
void exit_prog();
void load();
void unload();
void exporter();
void menu();


#endif
