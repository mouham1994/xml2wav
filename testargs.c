#include <stdio.h>
#include <stdlib.h>
#include "args.h"

int main(int argc, char** argv) {
  option_t* opt = NOOPTION;
  
  opt = opt_string(opt, "-a", f1);
  opt = opt_int(opt, "-b", f2);
  opt = opt_void(opt, "-c", f3);
  opt = opt_float(opt, "-d", f4);
 
  process_options(opt, argc, argv);
 
  opt_delete(opt);
 
  return EXIT_SUCCESS;
}
