#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "wave.h"
#include "partition.h"

#define prl { printf("\n"); }

void display(const char* fn) {
  FILE* fd = fopen(fn, "r");

  if (fd == NULL) {
    fprintf(stderr, "display: cannot open %s for reading\n", fn);
    return;
  }

  char buff[100];
  size_t in;

  while((in = fread(buff, 1, 100, fd)) > 0) {
    fwrite(buff, in, 1, stdout);
  }

  fclose(fd);
}


double signal_sine(double freq, double s) {
  return sin(2.0 * M_PI * freq * s);
}

double signal_violin(double freq, double s) {
  double r = 0.0;
  r += 0.995 * sin(2.0 * M_PI * 1.0 * freq * s);
  r += 0.940 * cos(2.0 * M_PI * 2.0 * freq * s);
  r += 0.425 * sin(2.0 * M_PI * 3.0 * freq * s);
  r += 0.480 * cos(2.0 * M_PI * 4.0 * freq * s);
  r += 0.365 * cos(2.0 * M_PI * 6.0 * freq * s);
  r += 0.040 * sin(2.0 * M_PI * 7.0 * freq * s);
  r += 0.085 * cos(2.0 * M_PI * 8.0 * freq * s);
  r += 0.090 * cos(2.0 * M_PI * 10.0 * freq * s);
  r *= 0.49;
  return r;
}


int main() {

  printf("*** Pitch implementation ***"); prl; prl;

  pitch_print(pitch(C,4)); prl;
  pitch_print(69); prl;
  printf("Octave of pitch 69: %hhd", pitch_octave(69)); prl;
  printf("Class of pitch 69: %d", pitch_class(69)); prl;
  printf("A0: %lf", pitch_frequency(pitch(A,0))); prl;
  printf("C4: %lf", pitch_frequency(pitch(C,4))); prl;
  printf("C8: %lf", pitch_frequency(pitch(C,8))); prl;

  prl;
  printf("*** Score implementation ***");
  prl;


  score_t* sc = score(100*Quarter, 1, 4*Quarter, 2);
  score_print_info(sc);
  prl;


  score_add_note(sc, 0, 0, pitch(C,4), 0*Eighth, Eighth);
  score_add_note(sc, 0, 0, pitch(D,4), 1*Eighth, Eighth);
  score_add_note(sc, 0, 0, pitch(E,4), 2*Eighth, Eighth);
  score_add_note(sc, 0, 0, pitch(F,4), 3*Eighth, Eighth);
  score_add_note(sc, 0, 0, pitch(G,4), 4*Eighth, Eighth);
  score_add_note(sc, 0, 0, pitch(A,4), 5*Eighth, Eighth);
  score_add_note(sc, 0, 0, pitch(B,4), 6*Eighth, Eighth);
  score_add_note(sc, 0, 0, pitch(C,5), 7*Eighth, Eighth);

  score_add_note(sc, 1, 0, pitch(C,2), 0*Eighth, Eighth);
  score_add_note(sc, 1, 0, pitch(G,2), 1*Eighth, Eighth);
  score_add_note(sc, 1, 0, pitch(C,2), 2*Eighth, Eighth);
  score_add_note(sc, 1, 0, pitch(F,2), 3*Eighth, Eighth);
  score_add_note(sc, 1, 0, pitch(C,2), 4*Eighth, Eighth);
  score_add_note(sc, 1, 0, pitch(F,2), 5*Eighth, Eighth);
  score_add_note(sc, 1, 0, pitch(G,2), 6*Eighth, Eighth);
  score_add_note(sc, 1, 0, pitch(C,2), 7*Eighth, Eighth);

  score_print_info(sc);
  prl;


  prl;
  printf("*** XML export of score ***");
  prl;
  prl;

  score_save_xml(sc, "c_major_scale.xml");
  display("c_major_scale.xml");


  prl;
  printf("*** XML import of score ***");
  prl;
  prl;

  score_t* sc2 = score_load_xml("au_clair_de_la_lune.xml");
  score_print_info(sc2);


  prl;
  printf("*** Instrument implementation (listen to it!) ***");
  prl;
  prl;

  instrument_t* sb[6];

  sb[0] = instrument_signal(signal_sine);

  sb[1] = instrument_signal(signal_violin);
  instrument_set_attack(sb[1], 0.08);
  instrument_set_duration(sb[1], 2.0);

  sb[2] = instrument_sample("313-grand_piano-c4-60.wav", 60);
  instrument_set_attack(sb[2], 0.002);
  instrument_set_duration(sb[2], 1.0);

  sb[3] = instrument_sample("318-grand_piano-d2-38.wav", 38);
  instrument_set_attack(sb[3], 0.002);
  instrument_set_duration(sb[3], 2.0);

  sb[4] = instrument_sample("316-grand_piano-c3-48.wav", 48);
  instrument_set_attack(sb[4], 0.002);
  instrument_set_duration(sb[4], 2.0);

  sb[5] = instrument_sample("132-cello-b2-59.wav", 59);
  instrument_set_attack(sb[5], 0.075);
  instrument_set_duration(sb[5], 2.0);

  wave_t* w = wave_create("ins.wav", 44100, 16, 1, 8*44100);
  for(size_t b = 0; b < 2*44100; b++) {
    double s = b / 44100.0;
    wave_set(w, 0, b, instrument_play(sb[0], pitch(A,4), s));
  }
  for(size_t b = 2*44100; b < 4*44100; b++) {
    double s = b / 44100.0 - 2.0;
    wave_set(w, 0, b, instrument_play(sb[1], pitch(A,4), s));
  }
  for(size_t b = 4*44100; b < 6*44100; b++) {
    double s = b / 44100.0 - 4.0;
    wave_set(w, 0, b, instrument_play(sb[2], pitch(A,4), s));
  }
  for(size_t b = 6*44100; b < 8*44100; b++) {
    double s = b / 44100.0 - 6.0;
    wave_set(w, 0, b, instrument_play(sb[3], pitch(A,2), s));
  }
  wave_close(w);

  prl;
  printf("*** WAVE synthesis of score (listen to it!) ***");
  prl;
  prl;

  score_export_wave(sc, sb+2, "c_major_scale.wav");
  score_delete(sc);

  score_export_wave(sc2, sb+1, "au_clair_de_la_lune.wav");
  score_delete(sc2);

  score_t* sc3 = score_load_xml("gymnopedie.xml");
  score_export_wave(sc3, sb+4, "gymnopedie.wav");
  score_delete(sc3);

  return EXIT_SUCCESS;
}
