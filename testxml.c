#include <stdlib.h>
#include "xml.h"
 
#define prl { printf("\n"); }
 
int main() {


  xelement_t* e = load_xml("testxml.xml");
  prl;
  print_xelement(e);
  
  save_xml("testxml_saved.xml", e);
  
  delete_xelement(e);
 
  return EXIT_SUCCESS;
}
