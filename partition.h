#ifndef __MIDI2WAV_PARTITION__
#define __MIDI2WAV_PARTITION__
 
 
#include "wave.h"
// Constant R = pow(2.0,1.0/12.0)
 
#define R 1.0594630943592953
 
// Types
 
typedef enum {
  C =  0,   // Do
  D =  2,   // Ré
  E =  4,   // Mi
  F =  5,   // Fa
  G =  7,   // Sol
  A =  9,   // La
  B = 11,   // Si
 
  Cs =  1,  // Do dièse
  Ef =  3,  // Mi bémol
  Fs =  6,  // Fa dièse
  Af =  8,  // La bémol
  Bf = 10,  // Si bémol
 
  Df =  1,  // Ré bémol
  Ds =  3,  // Ré dièse
  Gf =  6,  // Sol bémol
  Gs =  8,  // Sol dièse
  As = 10,  // La dièse
} pitchclass_t;
 
typedef unsigned char pitch_t;
 
// Constructors
 
pitch_t pitch(pitchclass_t pc, char octave);
 
// Accessors
 
char pitch_octave(pitch_t n);
 
pitchclass_t pitch_class(pitch_t n);
 
void pitch_print(pitch_t n);
 
double pitch_frequency(pitch_t n);
 
////////////////////////////////////////////////////////////////////////////////////////////
 
// Constants for ticks 
 
#define Whole     ((size_t)48)        // Ronde
#define Half      ((size_t)24)        // Blanche
#define Quarter   ((size_t)12)        // Noire
#define Eighth    ((size_t)6)         // Croche
#define Sixteenth ((size_t)3)         // Double croche
#define Triplet   ((size_t)4)         // Triolet
#define Sextuplet ((size_t)2)         // Sextolet
#define Dotted(n) ((size_t)(n + n/2))
 
typedef struct note_s {
	size_t start;
	size_t duration;
	pitch_t pitch;
	struct note_s* next;
} note_t;

typedef struct bar_s {
	note_t* notes;
} bar_t;

typedef struct staff_s {
	size_t size;
	bar_t* bars;
} staff_t;

// Types
typedef struct score_s {
	size_t tempo;
	size_t duration;
	size_t timeSignature;
	size_t nbInstrument;
	staff_t* instruments;
} score_t;
 
// Constructors
 
score_t* score(size_t tempo, size_t duration, size_t sig, size_t nbInstr);
 
void score_add_note(score_t* sc, size_t ins, size_t bar, pitch_t p, size_t start, size_t duration);
 
// Destructors
 
void score_delete(score_t* sc);
 
// Display
 
void score_print_info(score_t* sc);
 
// Input / output
 
void score_save_xml(score_t* sc, const char* fname);
 
score_t* score_load_xml(const char* fname);
 
////////////////////////////////////////////////////////////////////////////////////////////
 
// Types
 
typedef struct instrument_s {
	double vol;
	double attaque;
	double duration;
	int isWave;
	union {
		double (*sig)(double freq, double sec);
		wave_t* w;
	};
	pitch_t pitch;
} instrument_t;
 
// Constructors
 
instrument_t* instrument_signal(double (*sig)(double freq, double sec));
 
instrument_t* instrument_sample(const char* fname, pitch_t pitch_ref);
 
// Destructors
 
void instrument_delete(instrument_t* ins);
 
// Accessors / modifiers
 
void instrument_set_volume(instrument_t* ins, double volume);
 
void instrument_set_attack(instrument_t* ins, double attack);
 
void instrument_set_duration(instrument_t* ins, double duration);
 
double instrument_play(instrument_t* ins, pitch_t n, double s);
 
// Synthesis
 
void score_export_wave(score_t* sc, instrument_t** sounds_bank, const char* fname);
 
#endif
