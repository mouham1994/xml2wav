#ifndef __MIDI2WAV_XML__
#define __MIDI2WAV_XML__

#include <stdio.h>

typedef struct xattribute_s {
    char* name;
    char* value;
    struct xattribute_s* next_attribute;
} xattribute_t;

typedef struct xelement_s {
    char* name;
    xattribute_t* attributes;
    struct xelement_s* father;
    struct xelement_s* brother;
    int nbr_elems; // Si nbr_elems = -1 alors l'element contient un raw, sinon si nbr_elems >= 0 alors l'element contient des sous-elements
    union {
        char* raw;
        struct xelement_s* sons;
    };
} xelement_t;

// Constructors
xelement_t* create_xelement(const char* name);
void add_xattribute(xelement_t* e, const char* name, const char* value);
void add_sub_xelement(xelement_t* e, xelement_t* s);
void add_raw(xelement_t* e, const char* r);

// Destructors
void delete_xelement(xelement_t* e);

// Accessors
const char* get_xelement_name(xelement_t* e);
const char* get_xelement_attribute_value(xelement_t* e, const char* attribute_name);
xelement_t* get_xelement_children(xelement_t* e);
xelement_t* next_xelement_child(xelement_t* e);
const char* get_xelement_rawdata(xelement_t* e);

// Input/output
void save_xelement(FILE* fd, xelement_t* e);
void save_xml(const char* fname, xelement_t* e);
void print_xelement(xelement_t* e);

// Nouvelles fonctions
int is_space_char(char c);
int is_special_char_xml(char c);

char next_char(FILE* fd);
void check_next_char(FILE* fd, char c);
int is_next_char(FILE* fd, char c, int cons);
char* next_word(FILE* fd);
void check_next_word(FILE* fd, const char* w);
char* next_string(FILE* fd);
char* next_raw(FILE* fd);
// Fin Nouvelles fonctions


xelement_t* load_xml(const char* fname);
xelement_t* load_xelement(FILE* fd, const char* end_tag);
void load_xelement_raw(FILE* fd, xelement_t* e);
void load_xelement_sub(FILE* fd, xelement_t* e);
void load_xelement_content(FILE* fd, xelement_t* e);
xelement_t* load_xml(const char* fname);

#endif
